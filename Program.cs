﻿using System;

namespace ifelseifelse_example_task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var weather = (string) null;
            
            Console.WriteLine("\nHello world, what is the weather in your region or town?\n");
            
            wonderfulWeatherWeAreHaving:
            
            weather = Console.ReadLine();
            {
                if (weather.ToLower() == "sunny" || weather.ToLower() == "clear" || weather.ToLower() == "good")
                {
                    Console.WriteLine("\nThat's nice.\n");
                }
                else if (weather.ToLower() == "rainy" || weather.ToLower() == "cloudy" || weather.ToLower() == "bad")
                {
                    Console.WriteLine("\nOh no.\n");
                }
                else 
                {
                    Console.WriteLine("This one cannot comprehend your answer.\n Could you answer again? Is it sunny, cloudy, rainy or something along those lines?\n");
                    goto wonderfulWeatherWeAreHaving;
                }
            }
        }
    }
}
